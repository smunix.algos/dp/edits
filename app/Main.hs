module Main where

import           Data.Array.IArray     ((!))
import qualified Data.Array.IArray     as Arr
import qualified Data.Array.Unboxed    as UArr
import qualified Data.ByteString       as BS
import qualified Data.ByteString.Char8 as BC
import qualified Data.ByteString.Builder as BB
import           Data.Int
import Control.Monad (when, forever)
import System.IO (hFlush, stdout, stdin, hGetContents)
import System.Exit (exitSuccess)
import Data.List (minimumBy)
import Data.Ord (comparing)

naive :: (Eq a) => [a] -> [a] -> Int32
naive a b = d (length a) (length b)
  where
    d 0 0 = 0
    d 0 j = fromIntegral j
    d !i 0 = fromIntegral i
    d !i !j
      | a!!(i-1) == b!!(j-1) = d (i-1) (j-1)
      | otherwise = minimum [ d (i-1) j
                            , d i (j-1)
                            , d (i-1) (j-1)
                            ] + 1

basic :: String -> String -> Int32
basic a b = d (fromIntegral m) (fromIntegral n)
  where
    (m, n) = (fromIntegral $ length a, fromIntegral $ length b)
    d :: Int32 -> Int32 -> Int32
    d 0 0 = 0
    d !i 0 = i
    d 0 j = j
    d !i !j
      | a!!(fromIntegral $ i-1) == b!!(fromIntegral $ j-1) = ds!(i-1, j-1)
      | otherwise = minimum [ ds!(i-1, j)
                            , ds!(i, j-1)
                            , ds!(i-1, j-1)
                            ] + 1

    bounds :: ((Int32,Int32), (Int32, Int32))
    bounds = ((0,0),(m,n))

    ds :: Arr.Array (Int32,Int32) Int32
    ds = Arr.listArray bounds [ d i j | (i, j) <- Arr.range bounds]

data Action where
  None :: Char -> Action
  Add :: Char -> Action
  Rem :: Char -> Action
  Sub :: Char -> Char -> Action
  deriving (Show, Eq)

data Res where
  Res :: { _dist :: Int32
         , _actions :: [Action]
         } -> Res
  deriving (Show, Eq)

better :: (Action -> Int32) -> BC.ByteString -> BC.ByteString -> (Res, Arr.Array (Int32, Int32) Res)
better f a b =
  let
    (Res ed acts) = d m n
  in
    (Res ed (reverse acts), ds)
  where
    m, n :: Int32
    (m, n) = (fromIntegral $ BS.length a, fromIntegral $ BS.length b)
    d :: Int32 -> Int32 -> Res
    d 0 0 = Res 0 []

    d 0 !j =
      let
        (Res ed acts) = d 0 (j-1)
      in
        Res (ed+(f Add{})) ((Add (BC.index b (fromIntegral $ j-1))):acts)

    d !i 0 =
      let
        (Res ed acts) = d (i-1) 0
      in
        Res (ed+(f Rem{})) ((Rem (BC.index a (fromIntegral $ i-1))):acts)

    d !i !j
      | BC.index a (fromIntegral $ i-1) == BC.index b (fromIntegral $ j-1) =
        let
          (Res ed acts) = d (i-1) (j-1)
        in
          Res (ed+(f None{})) (None (BC.index a (fromIntegral $ i-1)) : acts)
      | otherwise =
          let
            ((Res mn acts), akt) = minimumBy (comparing (_dist . fst)) [ (ds!(i-1, j), Rem (BC.index a (fromIntegral $ i-1)))
                                                                       , (ds!(i-1, j-1), Sub (BC.index a (fromIntegral $ i-1)) (BC.index b (fromIntegral $ j-1)))
                                                                       , (ds!(i, j-1), Add (BC.index b (fromIntegral $ j-1)))
                                                                       ]
          in
            (Res (mn+(f akt)) (akt:acts))
    ds :: Arr.Array (Int32, Int32) Res
    ds = Arr.listArray bounds [d i j | (i,j) <- Arr.range bounds]

    bounds :: ((Int32, Int32), (Int32, Int32))
    bounds = ((0,0), (m, n))

main :: IO ()
main = forever $ do
  bytes <- rd
  process bytes
  where
    rd :: IO [BC.ByteString]
    rd = do
      l <- BS.hGetLine stdin
      (return . BC.words) l

    script :: BC.ByteString -> BC.ByteString -> IO ()
    script a b = do
      let
        (Res d acts, ds) = better (\case
                                      None{} -> 0
                                      _ -> 1) a b
        bb = BB.int32Dec d <> BB.string8 " => " <> BB.string8 (show acts)
      BB.hPutBuilder stdout bb
      BC.hPutStrLn stdout ""
      BC.hPutStrLn stdout $ BC.pack $ show ds
      hFlush stdout

    process :: [BC.ByteString] -> IO ()
    process [] = return ()
    process (a:[]) = do
      when (a == "q" || a == "quit")
        exitSuccess
      bytes <- rd
      process (a:bytes)
    process (a:b:ts) = do
      script a b
      process ts
